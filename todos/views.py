from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todo_lists(request):
    todolists = TodoList.objects.all()
    return render(request, "todo_lists/list.html", {"todolist_list": todolists})


def show_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    return render(request, "todo_lists/detail.html", {"todolist": todolist})


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
    return render(request, "todo_lists/create.html", {"form": form})


def edit_todo_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)

    else:
        form = TodoListForm(instance=todolist)

    context = {"form": form}

    return render(request, "todo_lists/edit.html", context)


def delete_todo_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todo_lists/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todo_items/create.html", {"form": form})


def edit_todo_item(request, id):
    todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)

    else:
        form = TodoItemForm(instance=todoitem)

    context = {"form": form}

    return render(request, "todo_items/edit.html", context)
